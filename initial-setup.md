# DevOps Supply Cloud Setup

- [DevOps Supply Cloud Setup](#devops-supply-cloud-setup)
  - [Intro](#intro)
  - [First SSH into droplet as Root User](#first-ssh-into-droplet-as-root-user)
  - [Creating a Sudo user](#creating-a-sudo-user)
  - [Installing Middleware/Technology stack](#installing-middlewaretechnology-stack)
    - [Node && NPM](#node--npm)
    - [Python && PIP](#python--pip)
    - [MongoDB](#mongodb)
    - [NGINX && Certbot](#nginx--certbot)
  - [What's Next](#whats-next)
    - [Optional Docs](#optional-docs)

#

## Intro

You will be assigned a digital ocean droplet by the TAs, please note the domain name and location of SSH key provided for the root user.

\*_note: whenever you text surrounded by {{text}} replace the text inside and remove the {{}}_

#

## First SSH into droplet as Root User

To ssh into the droplet for the first time, you will want to open the terminal or command prompt and enter the following command

```
ssh -i {{path/to/ssh/key}} root@{{domain-name}}
```

The ssh key path must be exact and must be the ssh key provided by the TA for the root user. After entering the command, you will now be inside the droplet and be looking at a similar screen to this.

```
Welcome to Ubuntu 20.04.2 LTS (GNU/Linux 5.4.0-67-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Fri Mar 19 01:41:39 UTC 2021

  System load:           0.0
  Usage of /:            14.8% of 24.06GB
  Memory usage:          27%
  Swap usage:            0%
  Processes:             108
  Users logged in:       0
  IPv4 address for eth0: {{IPv4-address}}
  IPv4 address for eth0: {{IPv4-address}}
  IPv6 address for eth0: {{IPv6-address}}
  IPv4 address for eth1: {{IPv4-address}}

15 update can be installed immediately.
0 of these updates are security updates.
To see these additional updates run: apt list --upgradable

* System Restart - required *
Last login: Wed Mar 17 01:14:41 2021 from 99.112.161.92
```

#

## Creating a Sudo user

It is generally not good practice to use the root user day-to-day. Root has the highest privileges in the server and using the root daily leaves the server vulnerable and causes a security concern. Yet there are many command that require root privileges. To remedy this, you will create a user and assign it sudo rights. With sudo rights, the user is able to run commands which typically only the root user could perform. This is done by adding `sudo` to the beginning of a command. Your password will be required, but the command will run.

Lets get on to creating the sudo user. While logged in as the root user, you will run the following command

- `adduser {{username}}`

After entering this command, you will be prompted for a password. Next you will be prompted for your full name and other info, this is optional and may be left blank. Now your user has been created and may be verified by entering `grep {{username}} /etc/passwd`

Now we will assign the user to the sudo group.

- `usermod -aG sudo {{username}}`

Finally, the server is set up only to accept ssh-key authentication, which means you must add your public key into your newly created user to access it.

Lets go over how to create your ssh key pair on your local machine. _Team 12 already created ssh key pairs, do not repeat this step below. Use the public key that already exists in you ~/.ssh/ directory._

**Make sure you are not logged into the droplet for this step.** On your terminal or command line, enter the following command to create the ssh key pair.

- `ssh-keygen -t rsa -b 4096`
- you will be prompted for a password, leave it blank just press enter

_Team 12 DevOps, now you will follow the steps here_

- next travel to the directory ~/.ssh/
- You will see two files id_rsa and id_rsa.pub
- Copy the contents of id_rsa.pub, this is your public key

With the public key copied to your clipboard, you will login to the droplet once more as root. `ssh -i {{path/to/ssh/key}} root@{{domain-name}}`. Now inside the server, you will copy the public key into the `authorized_keys` file of your sudo user.

- While logged in as the root user you will run the following commands

```bash
#switch user context to your sudo user
su - {{username}}

#create .ssh dir where your authorized_keys file will live, you will also give yourself ownership fo the dir
sudo mkdir ~/.ssh
sudo chown -R {{username}}:{{username}} ~/.ssh
sudo chmod 700 ~/.ssh

#travel into the dir and create the file using vim
cd ~/.ssh
vim authorized_keys
###copy the public key here### remember to press `i` to enter input mode and after copying the key into the file press `ESC` and type `:wq` to write the changes and quit
```

Now you can exit the user context by typing `logout` and logout from root by typing `logout` once more.

Verify your sudo user is created by logging into the user with the following command on your command line

- `ssh {{username}}@{{domain-name}}`

You should now have been able to login. Verify as well you have sudo privileges by running any command with sudo added to it

- `sudo ls`
  _if you have sudo privileges, it shoudl ask you for your password and print out all files/folders in your current directory_

#

## Installing Middleware/Technology stack

More likely than not, the first time you login, you will see multiple updates are available and a System restart is required. This could be a good moment to get the server up-to-date.

Run the following commands

```bash
#make sure your server has fetched all available updates and then upgrade them with the following commands
sudo apt update
sudo apt upgrade

#you will be prompted `After this operation, {{disk-space}} of additional disk space will be used. Do you want to continue? [Y/n]` type `Y`
y

#finally, lets reboot the system to get rid of the *System Restart required*
sudo reboot

#you will be kicked out, after a few minutes you will be able to login once more
```

Now that the server is up-to-date, we can begin installing our technology stack

- [Node v14.16.0](#node--npm)
- [NPM v6.14.11](#node--npm)
- [Python v3.8.5](#python--pip)
- [PIP v20.0.2](#python--pip)
- [MongoDB v4.4.3](#mongodb)
- [NGINX v1.18.0 (Ubuntu)](#nginx--certbot)
- [Certbot](#nginx--certbot)

#

### Node && NPM

```bash
sudo apt update
sudo apt install nodejs
sudo apt install npm

node -v
npm -v
#if version numbers appear, you have successfully installed node and npm
```

### Python && PIP

```bash
#python should already be installed, verify by running the following command, you should see a version number
python3 -V

#now lets install pip, the python package manager
sudo apt update
sudo apt install --yes python3-pip

#TA recommends the following packages to make our python environment robust (look into it if you have a chance and add info on them)
sudo apt install -y build-essential libssl-dev libffi-dev python3-dev

#finally lets install the python package for virtual environments
sudo apt install --yes python3-venv
```

### MongoDB

```bash
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
#you should see `OK` printed onn the terminal

#create the list file for MongoDB
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

#reload the package manager once more
sudo apt update

#now install mongodb
sudo apt-get install -y mongodb-org

#finally run the following to prevent unintended upgrades from occurring
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
```

### NGINX && Certbot

```bash
#lets update the package manager and install nginx with the following commands
sudo apt update
sudo apt install nginx-full
sudo apt install certbot python3-certbot-nginx

#verify with these commands
nginx -V
sudo systemctl status nginx
```

nginx-full is one of the various nginx flavors, you can look into the different kinds with this thread shared by the TA, [nginx-flavors](https://askubuntu.com/questions/553937/what-is-the-difference-between-the-core-full-extras-and-light-packages-for-ngi)

View the default nginx page by following these steps

- in the droplet terminal, enter the following command to get your IP address
  - `curl -4 icanhazip.com`
- copy the IP address into your browser like this
  - `http://{{ip-address}}`

#

## What's Next

Complete these preferrably in this order

- [MongoDB Config Docs](mongodb-docs.md)
- [Cloning Frontend/Backend Repos](cloning-repos.md)
- [Serving Frontend on Nginx and Setting up https with Certbot](nginx-config.md)
- [Systemd Service for Backend python server and reverse-proxy in Nginx](systemd-setup.md)

### Optional Docs

- [Building Bash Script to Update Frontend/Backend](bash-scripts.md)
