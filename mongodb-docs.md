# MongoDB Docs

- [MongoDB Docs](#mongodb-docs)
  - [Intro](#intro)
  - [Starting MongoDB Service](#starting-mongodb-service)
  - [Access Control with MongoDB](#access-control-with-mongodb)
    - [Create MongoDB Admin User](#create-mongodb-admin-user)
    - [Enable Access Control in mongod.conf](#enable-access-control-in-mongodconf)
    - [Open Mongo Shell with Admin User](#open-mongo-shell-with-admin-user)
    - [Create MongoDB Developer User](#create-mongodb-developer-user)
    - [Open Mongo Shell with Developer User](#open-mongo-shell-with-developer-user)

## Intro

Ideally, you will have already installed mongoDB during the initial setup docs. Verify this by running the command `mongo --version`. You should see the following

```bash
MongoDB shell version v4.4.3
Build Info: {
    "version": "4.4.3",
    "gitVersion": "913d6b62acfbb344dde1b116f4161360acd8fd13",
    "openSSLVersion": "OpenSSL 1.1.1f  31 Mar 2020",
    "modules": [],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "ubuntu2004",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}
```

If not, please go back to the initial setup and complete the install of mongodb

#

## Starting MongoDB Service

Mongo DB will be started by using the systemctl command on linux. This command is used to manage system services on the server.

```bash
sudo systemctl start mongod
sudo systemctl enable mongod

#check the status of mongo by running this command
sudo systemctl status mongod

#__OUTPUT__
● mongod.service - MongoDB Database Server
     Loaded: loaded (/lib/systemd/system/mongod.service; enabled; vendor preset>
     Active: active (running) since Fri 2021-03-19 19:00:30 UTC; 4min 27s ago
       Docs: https://docs.mongodb.org/manual
   Main PID: 19146 (mongod)
     Memory: 162.5M
     CGroup: /system.slice/mongod.service
             └─19146 /usr/bin/mongod --config /etc/mongod.conf
```

## Access Control with MongoDB

The TA shared a document for understanding the [principle of least privileges](https://us-cert.cisa.gov/bsi/articles/knowledge/principles/least-privilege#:~:text=The%20Principle%20of%20Least%20Privilege%20states%20that%20a%20subject%20should,control%20the%20assignment%20of%20rights.) to explain the layer of authentication we are about to implement.

MongoDB does not come with authentication enabled, we will want to enable it and add two users. An admin user and a developer user.

### Create MongoDB Admin User

At this point, you will be able to access mongo without authentication. Open mongo shell by running the command

`mongo --port 27017`

_(it is not always necessary to specify the port unless the default port, 27017, is not being used to run mongod)_

With shell open, you will create the admin user by entering the following lines.

```
#switch to the admin database with
use admin

db.createUser({
    user: "admin",
    pwd: "team12_admin",
    roles: [ { role: "root", db: "admin" } ]
})
```

You can verify by running the command `db.getUsers()`, if admin was created, you can now exit by typing `exit`

### Enable Access Control in mongod.conf

You will want to open the mongod.conf file. To do this, run the following command

`sudo vim /etc/mongod.conf`

The file should open up in the vim editor. Now in the file, find the line that says

```
#security:
#   authorization:enabled
```

Uncomment it to look like this _(Recal you must type `i` to enter input mode for this)_

```
security:
   authorization:enabled
```

Close out of vim by first escaping input mode with `ESC` and then typing `:wq` to save and exit. Now restart MongoDB by running

`sudo systemctl restart mongod`

### Open Mongo Shell with Admin User

At this point, you will need to open shell with the admin user to be able to make changes on the database. Open the shell with this command

```
mongo -u 'admin' --authenticationDatabase 'admin'
```

You will be prompted for the password we set earlier, [here](#create-mongodb-admin-user), enter it and mongo shell will open.

### Create MongoDB Developer User

As you recall, we used the admin db as the authentication db for the admin user. For the developer user, we will use a database specific for the cloud we are working with. For instance, in the demand server, we used a database called `team12_demand`, for the supply server we will call it `team12_supply`

```
use team12_{{cloud-name}}
db.createUser({
    user: "developer",
    pwd: "team12_developer",
    roles: ["readWrite"]
})
```

User is now created, exit mongo shell by typing `exit`

### Open Mongo Shell with Developer User

```
mongo -u 'developer' --authenticationDatabase 'team12_demand'
```
